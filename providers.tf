terraform {
  required_providers {
    ec = {
      source = "elastic/ec"
      version = "0.9.0"
    }
    google = {
      source = "hashicorp/google"
      version = "5.14.0"
    }
     kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.25.2"
    }
     elasticstack = {
      source = "elastic/elasticstack"
      version = "0.11.0"
    }
      kubectl = {
      source = "alekc/kubectl"
      version = "2.0.4"
    }
  }
}

provider "ec" {
  # Configuration options
}

provider "google" {
  project = var.project_id
  region  = var.region
}

/* provider "kubernetes" {
  // load_config_file = "false"

  host     = google_container_cluster.primary.endpoint
  username = var.gke_username
  password = var.gke_password

  client_certificate     = google_container_cluster.primary.master_auth.0.client_certificate
  client_key             = google_container_cluster.primary.master_auth.0.client_key
  cluster_ca_certificate = google_container_cluster.primary.master_auth.0.cluster_ca_certificate
 } */

 provider "elasticstack" {
 elasticsearch {
    endpoints = [ec_deployment.elastic_cloud.elasticsearch.https_endpoint]
    username  = ec_deployment.elastic_cloud.elasticsearch_username
    password  = ec_deployment.elastic_cloud.elasticsearch_password
  }
   kibana {
    endpoints =  [ec_deployment.elastic_cloud.kibana.https_endpoint]
  }
 }

 provider "kubectl" {
  host     = google_container_cluster.primary.endpoint
  username = var.gke_username
  password = var.gke_password

  client_certificate     = google_container_cluster.primary.master_auth.0.client_certificate
  client_key             = google_container_cluster.primary.master_auth.0.client_key
  cluster_ca_certificate = google_container_cluster.primary.master_auth.0.cluster_ca_certificate
}