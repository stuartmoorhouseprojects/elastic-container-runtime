# Retrieve the latest stack pack version
data "ec_stack" "latest" {
  version_regex = "latest"
  region        = "us-east-1"
}

# Create an Elastic Cloud deployment
resource "ec_deployment" "elastic_cloud" {
  # Optional name.
  name = "container runtime demo"

  region                 = "us-east-1"
  version                = data.ec_stack.latest.version
  deployment_template_id = "aws-io-optimized-v2"

  elasticsearch = {
    hot = {
      autoscaling = {}
    }
  }

  kibana = {}

  enterprise_search = {}

  integrations_server = {}

}

 output "kibana_endpoint" {
  value = ec_deployment.elastic_cloud.kibana.https_endpoint
  }

 output "elasticsearch_endpoint" {
  value = ec_deployment.elastic_cloud.elasticsearch.https_endpoint
  }