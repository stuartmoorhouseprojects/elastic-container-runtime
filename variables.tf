
variable "project_id" {
  type = string
}

variable "region" {
  type = string
}

variable "container_cluster_name" {
  type = string
}

# variable "gke_username" {
#   type = string
# }

# variable "gke_password" {
#   type = string
# }

/* variable "fleet_url" {
  type = "string"
}

variable "fleet_password" {
  type = "string"
} */

