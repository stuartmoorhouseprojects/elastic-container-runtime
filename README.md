# Elastic Container Runtime protection demonstration
## prerequesites
To run this demo you will need to have the following installed on your local machine:

* gcloud command line tool
* terraform

You will also need:

* An Elastic Cloud account
* A Google Cloud account

## create the resources
### authentication
gcloud auth application-default login

### applying the Terraform 
terraform init
terraform plan
terraform apply --auto-approve

## connect to the K8s cluster
 gcloud container clusters get-credentials $(terraform output -raw kubernetes_cluster_name) --region $(terraform output -raw region)

## monitor the K8s cluster
In another terminal:


