resource "elasticstack_fleet_agent_policy" "k8s" {
  name            = "k8s"
  namespace       = "default"
  description     = "kubernetes monitoring policy"
  sys_monitoring  = true
  monitor_logs    = true
  monitor_metrics = true
}

data "elasticstack_fleet_enrollment_tokens" "k8s-sample" {
  policy_id = elasticstack_fleet_agent_policy.k8s.policy_id
}

// KSPM
resource "elasticstack_fleet_integration" "kspm" {
  name    = "cloud_security_posture"
  version = "1.7.4"
  force   = false
}

resource "elasticstack_fleet_integration_policy" "kspm_plicy" {
  name                = "kspm"
  namespace           = "default"
  description         = "Kubernetes Security Posture Management (KSPM)"
  agent_policy_id     = elasticstack_fleet_agent_policy.k8s.policy_id
  integration_name    = elasticstack_fleet_integration.kspm.name
  integration_version = elasticstack_fleet_integration.kspm.version

  input {
    input_id = "kspm-cloudbeat/cis_k8s"
    streams_json = jsonencode({
        "cloud_security_posture.findings": {
        "enabled": true
        }
    })
  }

  vars_json = jsonencode({
    "posture": "kspm",
    "deployment": "self_managed"
  })

   }


// Container Defend



